<?php
namespace App\Http\Controllers\Blog;
use App\Http\Controllers\Controller as GuestBaseController;

abstract class BaseController extends GuestBaseController
{
    /**
     * BaseController constructor.
     */
    public function __construct(){
    }
}
